package modelo;

public class ModeloCliente {
    
    //Declaracion de variables que tendra la persona Cliente
    private String nombre;
    private String apellido;
    private String direccion;
    private String telefono;

    public ModeloCliente() {
    }
    
    //Constructor que inicializa las variables de clase
    public ModeloCliente(String nombre, String apellido, String direccion, String telefono) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.direccion = direccion;
        this.telefono = telefono;
    }

    //Geters y Seters de las variables de clase
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
   
}
