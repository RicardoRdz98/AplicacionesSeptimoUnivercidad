package modelo;

import singleton.Singleton;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProductoDAO extends DAO<ModeloProducto>{

    public ProductoDAO(Singleton conexion) {
        super(conexion);
    }

    @Override
    public void add(ModeloProducto obj) {
        
         try {

          indice = 1;
          //Instancia que resive una cadena de MySQL con los datos del Modelo de producto
          ps = conexion.obtenerConexion().prepareCall("insert into productos values(0,?,?)");
          ps.setString(indice++, obj.getNombre());
          ps.setDouble(indice++, obj.getPrecio());

          //Instruccion para ejecutar la instruccion de MySQL
          ps.execute();

        } catch (SQLException ex) {
          Logger.getLogger(ProveedoresDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @Override
    public void edit(ModeloProducto obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void remove(ModeloProducto obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ResultSet consultar(String storedProcedure) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
