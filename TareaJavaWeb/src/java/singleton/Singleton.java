package singleton;

import java.sql.DriverManager;
import java.sql.SQLException;

public class Singleton {
// 1. Atributo encapsulado de clase del mismo tipo de la clase

  private static Singleton cnn;
  private java.sql.Connection connection;

  // 2. Constructor privado
  private Singleton() throws ClassNotFoundException, SQLException{
    
    // Localizando a la clase de conexión dentro del paquete
    Class.forName("com.mysql.jdbc.Driver");
            
    // Generamos la conexión con la base de datos
    connection = DriverManager.getConnection(
            "jdbc:mysql://localhost/puntoventa",
            "root",
            ""
    );
        
  }

  // 3. Método de clase público con tipo de retorno del mismo tipo de la clase
  public static Singleton crearConexion() throws ClassNotFoundException, SQLException {

    // 4. Control sobre el número de instancias
    if (cnn == null) {
      cnn = new Singleton();
    }

    return cnn;

  }
  
  public java.sql.Connection obtenerConexion(){
    return connection;
  }
}
