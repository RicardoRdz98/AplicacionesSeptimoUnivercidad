<%@page contentType="text/html" pageEncoding="UTF-8"%>

<style>
    .centrado {
        position: center;
        left: 50%;
        top: 50%;
        transform: translate(-50%, -50%);
        -webkit-transform: translate(-50%, -50%);
    }
    .alineado{
        text-align: center;
    }
</style>

<!DOCTYPE html>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!--BOOTSTRAP-->
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div class="card centrado col-sm-auto">
            <div class="card-header">
                Insertar un Producto
            </div>
            <div class="card-body">
                <form method="POST" action="producto.run">
                    <input type="text" name="nombre" placeholder="Introduce un nombre">
                    <input type="number" name="precio" placeholder="Introduce un precio">
                    <br>
                    <div>
                        <button>Subir</button>
                    </div>
                </form>
            </div> 
        </div>
</body>
</html>
