/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo01;

/**
 *
 * @author rodri
 */
public class Alumno extends Persona {
    private String grupo;
    private float promedio;
    private String tutor;

    public Alumno() {
    }

    public Alumno(String grupo, float promedio, String tutor, int id, String nombre, String aPaterno, String aMaterno, String direccion) {
        super(id, nombre, aPaterno, aMaterno, direccion);
        this.grupo = grupo;
        this.promedio = promedio;
        this.tutor = tutor;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public float getPromedio() {
        return promedio;
    }

    public void setPromedio(float promedio) throws Exception{
        if(promedio >= 0){
            this.promedio = promedio;
        }else{
            throw new Exception(Mensajes.PROMEDIO_NEG);
        }
    }

    public String getTutor() {
        return tutor;
    }

    public void setTutor(String tutor) {
        this.tutor = tutor;
    }
    
    
}
