/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces2.pkg2;

/**
 *
 * @author rodri
 */
//Implementacion de la interface Nomina a la clase Ventas
public class Ventas implements Nomina{

    //Variable ha utilizar
    private int numVentas;

    //Sobreargando el constructor con su respectiva variable
    public Ventas(int numVentas) {
        this.numVentas = numVentas;
    }

    //Get y Set de la variable
    public int getNumVentas() {
        return numVentas;
    }

    public void setNumVentas(int numVentas) {
        this.numVentas = numVentas;
    }    
    
    //Metodo abstracto de la interfaz Nomina
    @Override
    public float calcularNomina() {
        float comicion = (float) (5000 * .05);
        float res = comicion * numVentas;
        return 5000 + res;
    }
    
}
