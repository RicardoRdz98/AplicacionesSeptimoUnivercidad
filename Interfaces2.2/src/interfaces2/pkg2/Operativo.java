/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces2.pkg2;

/**
 *
 * @author rodri
 */
//Implementacion de la interface Nomina a la clase Operativo
public class Operativo implements Nomina{

    //Variables ha utilizar
    private int numHoras, numHorasExtra;

    //Sobreargando el constructor con sus respectivas variables
    public Operativo(int numHoras, int numHorasExtra) {
        this.numHoras = numHoras;
        this.numHorasExtra = numHorasExtra;
    }

    //Geters y Seters de las variables
    public int getNumHoras() {
        return numHoras;
    }

    public void setNumHoras(int numHoras) {
        this.numHoras = numHoras;
    }

    public int getNumHorasExtra() {
        return numHorasExtra;
    }

    public void setNumHorasExtra(int numHorasExtra) {
        this.numHorasExtra = numHorasExtra;
    }
    
    //Metodo abstracto de la interfaz Nomina
    @Override
    public float calcularNomina() {
        int extras;
        if(numHorasExtra > 0){
           extras =  numHorasExtra * 17;
        }else{
            extras = 0;
        }
        return 38 * numHoras * 40 + extras;
    }
 
}
