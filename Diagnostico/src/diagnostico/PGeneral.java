/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diagnostico;

/**
 *
 * @author rodri
 */
public class PGeneral extends Persona{ //clase heredando de la clase padre Persona
    //Declaracion de variables
    private String nVoleto;
    private String nAsiento;

    //Declaracion de constructor para llenado de datos
    public PGeneral(String nombre, String aMaterno, String aPaterno, String nVoleto, String nAsiento) {
        super(nombre, aMaterno, aPaterno);
        this.nVoleto = nVoleto;
        this.nAsiento = nAsiento;
    }

    //Get y Set de cada uno de los atributos de la clase
    public String getnVoleto() {
        return nVoleto;
    }

    public void setnVoleto(String nVoleto) {
        this.nVoleto = nVoleto;
    }

    public String getnAsiento() {
        return nAsiento;
    }

    public void setnAsiento(String nAsiento) {
        this.nAsiento = nAsiento;
    }

    @Override
    public String toString() {
        return "Numero Voleto= " + nVoleto + ", Numero Asiento=" + nAsiento;
    }
    
}
