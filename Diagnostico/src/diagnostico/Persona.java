/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diagnostico;

/**
 *
 * @author rodri
 */
public class Persona { //clase padre Persona
    //Declaracion de variables que heredaraa sus clases hijas
    private String nombre;
    private String aMaterno;
    private String aPaterno;

    public Persona(String nombre, String aMaterno, String aPaterno) {
        this.nombre = nombre;
        this.aMaterno = aMaterno;
        this.aPaterno = aPaterno;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getaMaterno() {
        return aMaterno;
    }

    public void setaMaterno(String aMaterno) {
        this.aMaterno = aMaterno;
    }

    public String getaPaterno() {
        return aPaterno;
    }

    public void setaPaterno(String aPaterno) {
        this.aPaterno = aPaterno;
    }

    @Override
    public String toString() {
        return "Nombre= " + nombre + ", Apellido Materno= " + aMaterno + ", Apellido Paterno= " + aPaterno;
    }

}
