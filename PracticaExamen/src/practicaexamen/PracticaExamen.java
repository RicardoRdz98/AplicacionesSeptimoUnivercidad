
package practicaexamen;

/**
 *
 * @author rodri
 */
public class PracticaExamen {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int contador = 0;
        
        Figuras[] f = new Figuras[3];
        
        f[contador++] = new Cubo(4); //64
        f[contador++] = new Piramide(5, 10); // 16.6
        f[contador++] = new Esfera(7); //1436.7584
       
        float sumatoria = 0;
         for (Figuras figuras : f) {
             sumatoria += figuras.Calcular_volumen();
         }
         
         System.out.println("Total de la sumatoria de volumenes: " + sumatoria);
    }
    
}
