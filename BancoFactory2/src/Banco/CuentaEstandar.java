
package Banco;

public class CuentaEstandar extends Cuenta{

    //Declaracion de las variables globales
    private double interes = 0.005;
    private String regalo = "No hay regalo";

  public CuentaEstandar() {
  }

  //Inicializacion de las variables
  public CuentaEstandar(int edad, String nombre, String direccion, boolean nomina, boolean pension, double valor) {
    super(edad, nombre, direccion, nomina, pension, valor);
  }    

  public String getRegalo() {
    return regalo;
  }

  public double getInteres() {
    return interes * 100;
  }

  public double cotizar(double valor) {
    
    valor += valor * interes;  
    
    return valor;

  }
  
  
  
}
