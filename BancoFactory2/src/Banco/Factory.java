/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Banco;

/**
 *
 * @author rodri
 */
public class Factory {

    public static Cuenta extender(int edad, double valor, String valor2) {
        if (edad >= 18 && edad <= 25 && valor2.equals("NINGUNA")) {

            /*Cuenta c = new CuentaJoven();
              double total = c.cotizar(valor);
              CuentaJoven c1 = new CuentaJoven();
              double interes = c1.getInteres();
              txt_taza_interes.setText(interes + "%");
              txt_obsequio.setText(c1.getRegalo());
              txt_salida.setText(total + "");
              txt_cuenta.setText("Cuenta Joven");*/
            return new CuentaJoven();

            //If que decidira si es cuenta 10
        } else if (edad > 25 && edad < 65 && valor2.equals("NÓMINA")) {

            /*Cuenta c = new Cuenta10();
              double total = c.cotizar(valor);
              Cuenta10 c1 = new Cuenta10();
              double interes = c1.getInteres();
              txt_salida.setText(total + "");
              txt_cuenta.setText("Cuenta 10");
              txt_taza_interes.setText(interes + "%");
              txt_obsequio.setText(c1.getRegalo());*/
            return new Cuenta10();

            //If que decidira que es cuenta Oro
        } else if (edad >= 65 && valor2.equals("PENSIÓN")) {

            /*Cuenta c = new CuentaOro();
              double total = c.cotizar(valor);
              CuentaOro c1 = new CuentaOro();
              double interes = c1.getInteres();
              txt_salida.setText(total + "");
              txt_cuenta.setText("Cuenta Oro");
              txt_taza_interes.setText(interes + "%");
              txt_obsequio.setText(c1.getRegalo());*/
            return new CuentaOro();

            //Termino de If que decidira que es cuenta estandar
        } else {

            /*Cuenta c = new CuentaEstandar();
              double total = c.cotizar(valor);
              CuentaEstandar c1 = new CuentaEstandar();
              double interes = c1.getInteres();
              txt_salida.setText(total + "");
              txt_cuenta.setText("Cuenta estandar");
              txt_taza_interes.setText(interes + "%");
              txt_obsequio.setText(c1.getRegalo());*/
            return new CuentaJoven();
        }
    }

}
