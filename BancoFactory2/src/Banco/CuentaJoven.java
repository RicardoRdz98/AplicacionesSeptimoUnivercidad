
package Banco;

public class CuentaJoven extends Cuenta{

  // Se declaran e inicializan los valores que pueden cambiar y no se traeran desde la vista.
    private double interes = 0.02;
    private String regalo = "CD Música";

  public CuentaJoven() {
  }

  //No se van a modificar las variables de las clases hijos.
  public CuentaJoven(int edad, String nombre, String direccion, boolean nomina, boolean pension, double valor) {
    super(edad, nombre, direccion, nomina, pension, valor);
  }    

  //Declaracion de gets and Seters
  public String getRegalo() {
    return regalo;
  }

  public double getInteres() {
    return interes * 100;
  }
  
  public double cotizar(double valor) {
    
    valor += valor * interes;  
    
    return valor;

  }
  
  
  
}
