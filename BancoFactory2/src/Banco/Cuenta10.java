
package Banco;

public class Cuenta10 extends Cuenta {
    //Declaracion de variables para la clase
    private double interes = 0.01;
    private String regalo = "Reproductor de CD";

    public Cuenta10() {
    }

//Inicializacion de las variables
  public Cuenta10(int edad, String nombre, String direccion, boolean nomina, boolean pension, double valor) {
    super(edad, nombre, direccion, nomina, pension, valor);
  }    

  public String getRegalo() {
    return regalo;
  }

  public double getInteres() {
    return interes * 100;
  }

  public double cotizar(double valor) {
    
    valor = (valor += valor * interes);  
    
    return valor;

  }
  
}
